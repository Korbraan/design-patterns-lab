package eu.telecomnancy.helpers;

import java.io.IOException;
import java.util.Properties;

public class ReadPropertyFile {
    public Properties readFile(String filename) throws IOException {
        Properties p = new Properties();
        p.load(getClass().getResourceAsStream(filename));
        return p;
    }

    public String getPropValues() throws IOException {
    	String result = "";
        ReadPropertyFile rp = new ReadPropertyFile();
        Properties p = rp.readFile("/eu/telecomnancy/app.properties");
        for (String i : p.stringPropertyNames()) {
        	result = p.getProperty(i);
        }
		return result;


    }
}
