package eu.telecomnancy;

import java.io.IOException;

import eu.telecomnancy.helpers.ReadPropertyFile;
import eu.telecomnancy.sensor.*;
import eu.telecomnancy.ui.MainWindow;

public class SwingApp {

    public static void main(String[] args) {
    	
    	String sensorType = "";
		try {
			sensorType = (new ReadPropertyFile()).getPropValues();
		} catch (IOException e) {
			e.printStackTrace();
		}
        AbstractSensor sensor = new SensorFactory().getSensor(sensorType);
        new MainWindow(sensor);
    }
}
