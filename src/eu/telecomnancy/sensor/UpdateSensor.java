package eu.telecomnancy.sensor;

public class UpdateSensor implements SensorCommand {

	ISensor sensor;
	
	public UpdateSensor(ISensor sensor) {
		this.sensor = sensor;
	}
	
	@Override
	public void execute() {
		try {
			sensor.update();
		} catch (SensorNotActivatedException e) {
			e.printStackTrace();
		}
	}

}
