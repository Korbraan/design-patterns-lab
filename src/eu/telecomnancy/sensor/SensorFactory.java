package eu.telecomnancy.sensor;

public class SensorFactory {
	public static AbstractSensor sensor;
	
	public AbstractSensor getSensor(String sensorType) {
		if(sensorType.contains("LegacyTemperatureSensor")) {
			return new LegacyTemperatureSensorAdapter();
		} else if(sensorType.contains("ContextSensor")) {
			return new ContextSensor();
		} else if(sensorType.contains("ProxySensor")) {
			return new ProxySensor(new AbstractSensor());
		} else if(sensorType.contains("DecoratorSensorRound")) {
			return new DecoratorSensorRound(new AbstractSensor());
		} else if(sensorType.contains("DecoratorSensorFahr")) {
			return new DecoratorSensorFahr(new AbstractSensor());
		} else {
			return new TemperatureSensor();
		}
	}
	
}
