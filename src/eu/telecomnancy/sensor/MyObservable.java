package eu.telecomnancy.sensor;

import java.util.ArrayList;

import eu.telecomnancy.ui.MyObserver;

public abstract class MyObservable {
	protected ArrayList<MyObserver> observers = new ArrayList<MyObserver>();
	
	public void notifyObservers() {
		for (MyObserver o : observers) {
			o.update();
		}
	}
	public void addObserver(MyObserver o) {
		observers.add(o);
	}
	
	public void removeObserver(MyObserver o) {
		observers.remove(observers.indexOf(o));
	}
}
