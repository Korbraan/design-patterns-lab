package eu.telecomnancy.sensor;

public class DecoratorSensorFahr extends AbstractDecorator {

	public DecoratorSensorFahr(AbstractSensor sensor) {
		super(sensor);
	}
	
	@Override
	public double getValue() throws SensorNotActivatedException {
		 if (state) {
	        	return 1.8*value+32;
	        }     
	        else throw new SensorNotActivatedException("Sensor must be activated to get its value.");
	}
}
