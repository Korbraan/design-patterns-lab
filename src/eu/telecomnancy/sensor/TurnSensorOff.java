package eu.telecomnancy.sensor;

public class TurnSensorOff implements SensorCommand {
	
	ISensor sensor;
	
	public TurnSensorOff(ISensor sensor) {
		this.sensor = sensor;
	}
	
	@Override
	public void execute() {
		sensor.off();
	}

}
