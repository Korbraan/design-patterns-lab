package eu.telecomnancy.sensor;

public class AbstractDecorator extends AbstractSensor {
	
	AbstractSensor sensor;
	
	public AbstractDecorator(AbstractSensor sensor) {
		super();
		this.sensor = sensor;
	}
}
