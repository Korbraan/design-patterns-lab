package eu.telecomnancy.sensor;

public class LegacyTemperatureSensorAdapter extends AbstractSensor{
	LegacyTemperatureSensor sensor;
	
	public LegacyTemperatureSensorAdapter() {
		super();
		sensor = new LegacyTemperatureSensor();
	}
	
	@Override
	public void on() {
		if(sensor.getStatus() == false) {
			sensor.onOff();
		}
	}
	
	@Override
	public void off() {
		if(sensor.getStatus() == true) {
			sensor.onOff();
		}
	}
	
	@Override
	public void update() {
		this.off();
		this.on();
	}
	
	@Override
	public boolean getStatus() {
        return sensor.getStatus();
    }
	
	@Override
	public double getValue() throws SensorNotActivatedException {
        if (state)
            return sensor.getTemperature();
        else throw new SensorNotActivatedException("Sensor must be activated to get its value.");
    }
}
