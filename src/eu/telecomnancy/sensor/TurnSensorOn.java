package eu.telecomnancy.sensor;

public class TurnSensorOn implements SensorCommand {

	ISensor sensor;
	
	public TurnSensorOn(ISensor sensor) {
		this.sensor = sensor;
	}
	
	@Override
	public void execute() {
		sensor.on();
	}

}
