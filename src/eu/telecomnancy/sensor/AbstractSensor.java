package eu.telecomnancy.sensor;

import java.util.Random;

public class AbstractSensor extends MyObservable implements ISensor {
	protected boolean state;
    protected double value = 0;
    
    public AbstractSensor() {
    	super();
    }

    public void on() {
        state = true;
        this.notifyObservers();
    }

    public void off() {
        state = false;
        this.notifyObservers();
    }

    public boolean getStatus() {
        return state;
    }

    public void update() throws SensorNotActivatedException {
        if (state) {
        	 value = (new Random()).nextDouble() * 100;
        }  
        else throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
        this.notifyObservers();
    }

    public double getValue() throws SensorNotActivatedException {
        if (state) {
        	return value;
        }     
        else throw new SensorNotActivatedException("Sensor must be activated to get its value.");
    }
}
