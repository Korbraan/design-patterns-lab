package eu.telecomnancy.sensor;

public interface TemperatureSensorState {
	public void turnOn(ContextSensor c);
	public void turnOff(ContextSensor c);
	public boolean getStateOn();
}
