package eu.telecomnancy.sensor;

public class IsOff implements TemperatureSensorState{

	@Override
	public void turnOn(ContextSensor c) {
		c.setState(new IsOn());
		
	}

	@Override
	public void turnOff(ContextSensor c) {
		// Nothing to do
	}

	@Override
	public boolean getStateOn() {
		return false;
	}
}
