package eu.telecomnancy.sensor;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ProxySensor extends AbstractSensor{
	AbstractSensor sensor;
	
	DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	Date date = new Date();

	public ProxySensor(AbstractSensor sensor) {
		super();
		this.sensor = sensor;
	}
	
	@Override
	public void on() {
        this.sensor.on();
        this.notifyObservers();
        System.out.println(dateFormat.format(date) + "\nThe sensor has been turned on.\n");
    }

	@Override
    public void off() {
    	 this.sensor.off();
    	 this.notifyObservers();
         System.out.println(dateFormat.format(date) + "\nThe sensor has been turned off.\n");
    }

	@Override
    public boolean getStatus() {
    	boolean status = this.sensor.getStatus();
    	if(status == true) {
    		System.out.println(dateFormat.format(date) + "\nThe sensor is on.\n");
    	} else {
    		System.out.println(dateFormat.format(date) + "\nThe sensor is off.\n");
    	}
    	return status;
    }

	@Override
    public void update() throws SensorNotActivatedException {
        this.sensor.update();
        System.out.println(dateFormat.format(date) + "\nThe sensor has been updated.\n");
        this.notifyObservers();
    }

	@Override
    public double getValue() throws SensorNotActivatedException {
       double value = this.sensor.getValue();
       System.out.println(dateFormat.format(date) + "\nThe current value of the sensor is " + value + ".\n");
       return value;
    }
}
