package eu.telecomnancy.sensor;

import java.util.Random;

public class ContextSensor extends AbstractSensor  {
	private TemperatureSensorState state;
	
	public ContextSensor() {
		super();
		this.setState(new IsOff());
	}
	
	public void setState(TemperatureSensorState state) {
		this.state = state;
	}
	
	public TemperatureSensorState getState() {
		return state;
	}
	
	@Override
	public void on() {
		state.turnOn(this);
		this.notifyObservers();
	}
	
	@Override
	public void off() {
		state.turnOff(this);
		this.notifyObservers();
	}
	
	@Override
	public double getValue() throws SensorNotActivatedException {
        if (this.state.getStateOn()) {
        	return value;
        }     
        else throw new SensorNotActivatedException("Sensor must be activated to get its value.");
    }
	
	@Override
	public boolean getStatus() {
		if (this.state.getStateOn()) {
        	return true;
        } 
		return false;
    }
	
	@Override
	public void update() throws SensorNotActivatedException {
        if (this.state.getStateOn()) {
        	 value = (new Random()).nextDouble() * 100;
        }  
        else throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
        this.notifyObservers();
    }
}
