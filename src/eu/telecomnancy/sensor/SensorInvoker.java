package eu.telecomnancy.sensor;

public class SensorInvoker {

	SensorCommand command;
	
	public SensorInvoker (SensorCommand command) {
		this.command = command;
	}
	
	public void press() {
		this.command.execute();
	}
}
