package eu.telecomnancy.sensor;

public class IsOn implements TemperatureSensorState {

	@Override
	public void turnOn(ContextSensor c) {
		// Nothing to do
		
	}

	@Override
	public void turnOff(ContextSensor c) {
		c.setState(new IsOff());
	}

	@Override
	public boolean getStateOn() {
		return true;
	}
}
