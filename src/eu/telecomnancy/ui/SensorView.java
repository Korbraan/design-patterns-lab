package eu.telecomnancy.ui;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.MyObservable;
import eu.telecomnancy.sensor.SensorCommand;
import eu.telecomnancy.sensor.SensorInvoker;
import eu.telecomnancy.sensor.SensorNotActivatedException;
import eu.telecomnancy.sensor.TurnSensorOff;
import eu.telecomnancy.sensor.TurnSensorOn;
import eu.telecomnancy.sensor.UpdateSensor;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SensorView extends JPanel implements MyObserver {
    private ISensor sensor;

    private SensorCommand turnOn;
    private SensorCommand turnOff;
    private SensorCommand updateSensor;
    
    private SensorInvoker turnOnInvoker;
    private SensorInvoker turnOffInvoker;
    private SensorInvoker updateSensorInvoker;
    
    private JLabel value = new JLabel("N/A �C");
    private JButton on = new JButton("On");
    private JButton off = new JButton("Off");
    private JButton update = new JButton("Acquire");

    public SensorView(ISensor c) {
        this.sensor = c;
        ((MyObservable) c).addObserver(this);
        
        turnOn = new TurnSensorOn(sensor);
        turnOff = new TurnSensorOff(sensor);
        updateSensor = new UpdateSensor(sensor);
        
        turnOnInvoker = new SensorInvoker(turnOn);
        turnOffInvoker = new SensorInvoker(turnOff);
        updateSensorInvoker = new SensorInvoker(updateSensor);
        
        this.setLayout(new BorderLayout());

        value.setHorizontalAlignment(SwingConstants.CENTER);
        Font sensorValueFont = new Font("Sans Serif", Font.BOLD, 18);
        value.setFont(sensorValueFont);

        this.add(value, BorderLayout.CENTER);


        on.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	turnOnInvoker.press();
            }
        });

        off.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	turnOffInvoker.press();
            }
        });

        update.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                updateSensorInvoker.press();
            }
        });
        
        JPanel buttonsPanel = new JPanel();
        
        buttonsPanel.setLayout(new GridLayout(1, 3));
        buttonsPanel.add(update);
        buttonsPanel.add(on);
        buttonsPanel.add(off);
        

    this.add(buttonsPanel, BorderLayout.SOUTH);
        on.setEnabled(true);
		off.setEnabled(false);
		update.setEnabled(false);
    }

	@Override
	public void update() {
		if(this.sensor.getStatus() == true) {
			on.setEnabled(false);
			off.setEnabled(true);
			update.setEnabled(true);
			try {
				value.setText(String.valueOf(this.sensor.getValue()));
			} catch (SensorNotActivatedException e) {
				e.printStackTrace();
			}
		} else {
			on.setEnabled(true);
			off.setEnabled(false);
			update.setEnabled(false);
		}
	}
}
